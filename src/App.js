import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import { ToastContainer } from "react-toastify";
import AdminDashboard from "./components/admin/adminDashboard";
import LoginForm from "./components/user/login/loginForm";
import Home from "./components/user/home/home";
import ProtectedRoute from "./components/common/protectedRoute";
import Collection from "./components/user/collections/collection";
import "react-toastify/dist/ReactToastify.css";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <ToastContainer />
        <Switch>
          <ProtectedRoute path="/admin" component={AdminDashboard} />
          <Route path="/login" component={LoginForm} />
          <Switch>
            <Route path="/collection" component={Collection} />
            <Route path="/" component={Home} />
          </Switch>
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
