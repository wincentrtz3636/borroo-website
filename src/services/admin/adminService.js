import http from "../httpServices";
import config from "../../config.json";

export function getAllUsers() {
  return http.get(config.apiUrl + "/users");
}

export function getAllGenres() {
  return http.get(config.apiUrl + "/book-genres");
}

export function getAllCategories() {
  return http.get(config.apiUrl + "/book-categories");
}

export function getAllPublishers() {
  return http.get(config.apiUrl + "/book-publishers");
}

export function getAllAuthors() {
  return http.get(config.apiUrl + "/authors");
}
