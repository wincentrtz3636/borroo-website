import http from "./httpServices";
import config from "../config.json";

http.setJwt(getJwt());

export function getJwt() {
  return localStorage.getItem("token");
}
export function loginWithJwt(jwt) {
  localStorage.setItem("token", jwt);
}

export async function login(email, password) {
  const { data } = await http.post(config.apiUrl + "/login", {
    email: email,
    password: password
  });
  localStorage.setItem("token", data.auth_token);
}

export function logout() {
  localStorage.removeItem("token");
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem("token");
    return jwt;
  } catch (ex) {
    return null;
  }
}

export default {
  login,
  getCurrentUser,
  logout,
  loginWithJwt,
  getJwt
};
