import React, { Component } from "react";
import "./navbar.css";

import logo from "../../../assets/logo.png";

class Navbar extends Component {
  render() {
    return (
      <nav
        className={
          "navbar navbar-expand-lg navbar-light " + this.props.navbar.color
        }
      >
        <a
          style={{ display: this.props.navbar.logo, marginLeft: "20px" }}
          className="navbar-brand"
          href="#"
        >
          <img src={logo} style={{ width: "160px" }} alt="" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {this.props.navbar.left.map(nav => (
              <li key={nav.id} className="nav-item">
                {nav.content}
              </li>
            ))}
          </ul>
          <form className="form-inline my-2 my-lg-0">
            {this.props.navbar.right.map(nav => (
              <a key={nav.id} className="mr-3">
                {nav.content}
              </a>
            ))}
          </form>
        </div>
      </nav>
    );
  }
}

export default Navbar;
