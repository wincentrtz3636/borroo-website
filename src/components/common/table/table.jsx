import React, { Component } from "react";
import TableHeader from "../tableHeader/tableHeader";
import TableBody from "../tableBody/tableBody";

const Table = ({ datas, columns, sortColumn, onSort }) => {
  return (
    <div className="container-fluid table-responsive">
      <table className="table table-hover">
        <TableHeader
          columns={columns}
          sortColumn={sortColumn}
          onSort={onSort}
        />
        <TableBody columns={columns} data={datas.body} />
      </table>
    </div>
  );
};

export default Table;
