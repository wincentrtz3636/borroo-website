import React, { Component } from "react";
import "../navbar/navbar.css";

import logo from "../../../assets/logo.png";

class NavbarHome extends Component {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light transparent">
        <a
          style={{ display: "block", marginLeft: "20px" }}
          className="navbar-brand"
          href="#"
        >
          <img src={logo} style={{ width: "160px" }} alt="" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="#">
                About
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Collection
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Contact Us
              </a>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <button className="btn btn-register my-2 mr-3" type="submit">
              Register
            </button>
            <button
              onClick={this.props.onToggleModal}
              className="btn btn-login my-2 my-sm-0 mr-3"
              type="button"
            >
              Login
            </button>
          </form>
        </div>
      </nav>
    );
  }
}

export default NavbarHome;
