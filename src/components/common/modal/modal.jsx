import React from "react";
import Joi from "joi-browser";
import "./modal.css";
import Form from "../../common/form";
import auth from "../../../services/authServices";

class Modal extends Form {
  state = {
    data: { email: "", password: "" },
    errors: {}
  };

  schema = {
    email: Joi.string()
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { data } = this.state;
      await auth.login(data.email, data.password);
      window.location = "/admin";
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.email = ex.response.data;
        this.setState({ errors });
      }
    }
  };
  render() {
    return (
      <div
        class={"modal " + this.props.modal.condition}
        style={{ opacity: this.props.modal.opacity }}
        tabindex="-1"
        role="dialog"
      >
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" style={{ fontWeight: "bolder" }}>
                Login Page
              </h5>
              <button
                type="button"
                class="close"
                onClick={this.props.onToggleModal}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="form" onSubmit={this.handleSubmit}>
                {this.renderInput("email", "Email")}
                {this.renderInput("password", "Password", "password")}
                {this.renderButton("login")}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
