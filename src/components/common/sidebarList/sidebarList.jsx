import React from "react";
import { NavLink } from "react-router-dom";
import "./sidebarList.css";
const SidebarList = ({ list }) => {
  return (
    <div className="container-fluid sidebar-list">
      <NavLink
        className="nav-link"
        activeStyle={{ color: "white" }}
        exact={true}
        to={list.url}
      >
        <h6>
          <i className={list.icon + " sidebar-icon"} />
          {list.label}
        </h6>
      </NavLink>
    </div>
  );
};

export default SidebarList;
