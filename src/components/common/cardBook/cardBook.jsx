import React, { Component } from "react";
import "./cardBook.css";

class CardBook extends Component {
  render() {
    return (
      <div className="col-4">
        <div className="card">
          <div className="card-image">
            <img
              className="card-img-top"
              src={require("../../../assets/" + this.props.book.img)}
              alt="Card image cap"
            />
          </div>

          <div className="card-body card-movie">
            <h4
              className="card-title card-movie-title"
              style={{ marginBottom: "0px" }}
            >
              {this.props.book.title} ({this.props.book.year})
            </h4>

            <p className="card-text card-movie-text">
              {this.props.book.description}
            </p>
            <div className="row">
              <div className="col-6">
                <h5>
                  <span className="badge new-badge">New</span>
                </h5>
              </div>
              <div className="col-6">
                <h5 className="text-right">
                  <i className="fa fa-star" />
                  <i className="fa fa-star m-1" />
                  <i className="fa fa-star" />
                  <i className="fa fa-star m-1" />
                  <i className="fa fa-star" />
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardBook;
