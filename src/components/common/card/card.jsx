import React from "react";
import "./card.css";

const Card = ({ card }) => {
  return (
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div className="col-8">
            <h2>{card.label}</h2>
            <h1>0</h1>
          </div>
          <div className="col-2 text-right">
            <h1 className="card-icon">
              <i className={card.icon} />
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
