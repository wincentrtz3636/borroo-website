import React, { Component } from "react";
import "./sidebar.css";
import admin from "../../../assets/admin.jpg";
import SidebarList from "../sidebarList/sidebarList";

class Sidebar extends Component {
  render() {
    return (
      <div
        id="sidebar"
        className="col-2"
        style={{ maxWidth: this.props.status }}
      >
        <div className="row" id="sidebar-profile">
          <div id="profile" className="col-3">
            <img src={admin} className="rounded-circle" alt="" />
          </div>
          <div className="col-9">
            <h6>Wincent</h6>
            <h6>Admin</h6>
          </div>
        </div>
        <div className="container-fluid" id="navigation">
          <h6>Navigations</h6>
        </div>
        {this.props.sidebar.map(list => (
          <SidebarList list={list} key={list.label} />
        ))}
      </div>
    );
  }
}

export default Sidebar;
