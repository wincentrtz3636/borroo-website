import React, { Component } from "react";
import "./login.css";
import { NavLink } from "react-router-dom";
import Navbar from "../../common/navbar/navbar";
import logo from "../../../assets/logo.png";

class LoginForm extends Component {
  state = {
    navbar: {
      logo: "block",
      color: "transparent",
      left: [
        {
          id: 1,
          content: (
            <a className="nav-link" href="#">
              About
            </a>
          )
        },
        {
          id: 2,
          content: (
            <a className="nav-link" href="#">
              Collection
            </a>
          )
        },
        {
          id: 3,
          content: (
            <a className="nav-link" href="#">
              Contact Us
            </a>
          )
        }
      ],
      right: [
        {
          id: 1,
          content: (
            <button className="btn btn-register my-2 mr-3" type="submit">
              Register
            </button>
          )
        },
        {
          id: 2,
          content: (
            <NavLink
              to="/login"
              className="btn btn-login my-2 my-sm-0"
              type="submit"
            >
              Login
            </NavLink>
          )
        }
      ]
    }
  };
  render() {
    return (
      <div>
        <div id="login-wrapper" className="container-fluid primary">
          <Navbar navbar={this.state.navbar} />

          <div id="login-content">
            <div className="container" id="login-form">
              <input
                class="form-control form-control-lg"
                type="text"
                placeholder="Email"
              />
              <input
                class="form-control form-control-lg"
                type="text"
                placeholder="Password"
              />
              <button className="btn btn-lg btn-block" id="btn-register">
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginForm;
