import React, { Component } from "react";
import "./home.css";
import LandingPageSection from "./landingPageSection";
import CollectionPageSection from "./collectionPageSection";
import Modal from "../../common/modal/modal";
import AboutPageSection from "./aboutPageSection";
class Home extends Component {
  state = {
    common: {
      searchbox: "0"
    },
    data: {
      email: "",
      password: ""
    },
    cardBook: [
      {
        id: 1,
        title: "Book 1",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta recusandae aliquam nobis blanditiis dolor, sapiente non. Recusandae assumenda sed dolorem accusamus, voluptatum, ea, quod iste repellendus aliquam facere quam delectus!",
        year: 2018,
        img: "book3.jpg",
        rating: 5
      },
      {
        id: 2,
        title: "Book 2",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta recusandae aliquam nobis blanditiis dolor, sapiente non. Recusandae assumenda sed dolorem accusamus, voluptatum, ea, quod iste repellendus aliquam facere quam delectus!",
        year: 2018,
        img: "book4.jpg",
        rating: 5
      },
      {
        id: 3,
        title: "Book 3",
        description:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta recusandae aliquam nobis blanditiis dolor, sapiente non. Recusandae assumenda sed dolorem accusamus, voluptatum, ea, quod iste repellendus aliquam facere quam delectus!",
        year: 2018,
        img: "book5.jpg",
        rating: 5
      }
    ],
    modal: {
      condition: "",
      opacity: 0
    }
  };

  constructor() {
    super();
    this.searchToggle = this.searchToggle.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  searchToggle() {
    const common = {
      searchbox: "60%"
    };
    this.setState({ common });
  }

  toggleModal() {
    const modal = {
      condition: this.state.modal.condition === "" ? "show" : "",
      opacity: this.state.modal.opacity === 0 ? 1 : 0
    };
    this.setState({ modal });
  }

  render() {
    return (
      <div>
        <Modal modal={this.state.modal} onToggleModal={this.toggleModal} />
        <LandingPageSection
          onToggleModal={this.toggleModal}
          common={this.state.common}
          onToggle={this.searchToggle}
        />
        <AboutPageSection />
        <CollectionPageSection books={this.state.cardBook} />
      </div>
    );
  }
}

export default Home;
