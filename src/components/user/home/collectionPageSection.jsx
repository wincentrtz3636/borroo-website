import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import CardBook from "../../common/cardBook/cardBook";

class CollectionPageSection extends Component {
  render() {
    return (
      <div className="container-fluid" id="collection-section">
        <div className="row">
          <div className="col-6">
            <h1 className="section-title">Newest Collections</h1>
          </div>
          <div className="col-6 text-right view-more">
            <NavLink to="/collection" exact={true} className="section-title">
              View More <span style={{ fontWeight: "bolder" }}> > </span>
            </NavLink>
          </div>
        </div>

        <div className="row">
          {this.props.books.map(book => (
            <CardBook book={book} />
          ))}
        </div>
      </div>
    );
  }
}

export default CollectionPageSection;
