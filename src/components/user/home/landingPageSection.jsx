import React, { Component } from "react";
import NavbarHome from "../../common/navbarHome/navbarHome";
class LandingPageSection extends Component {
  render() {
    return (
      <div className="primary">
        <NavbarHome onToggleModal={this.props.onToggleModal} />
        <div className="container-fluid">
          <div className="row-home">
            <div className="col-6 landing-detail">
              <h1>
                Find & Borrow <br />
                Your Favorite Books
              </h1>
              <div
                className="input-group mt-3 mb-3"
                onMouseEnter={this.props.onToggle}
              >
                <input
                  type="text"
                  id="search-box"
                  className="form-control"
                  placeholder="Search Here..."
                  style={{ maxWidth: this.props.common.searchbox }}
                />
                <div className="input-group-append">
                  <button className="input-group-text" id="basic-addon2">
                    <span className="fa fa-search" />
                  </button>
                </div>
              </div>
            </div>
            <div className="col-6 image" />
          </div>
        </div>
      </div>
    );
  }
}

export default LandingPageSection;
