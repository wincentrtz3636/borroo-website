import React, { Component } from "react";
import find from "../../../assets/find.jpg";

class AboutPageSection extends Component {
  state = {};
  render() {
    return (
      <div className="container-fluid" id="about-section">
        <h1 className="section-title text-center">What is Borroo</h1>
        <div className="row" id="about-body">
          <div className="col-6">
            <img src={find} style={{ width: "100%" }} alt="" />
          </div>
          <div className="col-6">
            <h5>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae,
              alias ducimus explicabo voluptatem iste sit eum eos excepturi
              delectus? Magni officia praesentium nemo quod debitis, impedit
              voluptatibus qui optio quibusdam? Lorem ipsum dolor sit amet
              consectetur adipisicing elit. Saepe iusto delectus ratione
              consequatur dolore nemo placeat officiis cumque, ab deserunt
              commodi eveniet neque recusandae vel eum debitis tenetur sequi a!
            </h5>
          </div>
        </div>
      </div>
    );
  }
}

export default AboutPageSection;
