import React, { Component } from "react";
import "./style.css";
import Sidebar from "../common/sidebar/sidebar";
import Navbar from "../common/navbar/navbar";
import admin from "../../assets/admin.jpg";
import { Switch } from "react-router-dom";
import { toast } from "react-toastify";

import AdminContent from "./adminContent";
import AdminUserContent from "./adminUserContent";

import {
  getAllUsers,
  getAllGenres,
  getAllCategories,
  getAllPublishers,
  getAllAuthors
} from "../../services/admin/adminService";
import ProtectedRoute from "../common/protectedRoute";
import AdminGenreContent from "./adminGenreContent";
import AdminCategoryContent from "./adminCategoryContent";
import AdminPublisherContent from "./adminPublisherContent";
import AdminAuthorContent from "./adminAuthorContent";

class AdminDashboard extends Component {
  state = {
    sidebar: "267px",
    sidebarList: [
      {
        label: "Home",
        icon: "fa fa-home",
        url: "/admin"
      },
      {
        label: "Users",
        icon: "fa fa-users",
        url: "/admin/user"
      },
      {
        label: "Genre",
        icon: "fa fa-genderless",
        url: "/admin/genre"
      },
      {
        label: "Category",
        icon: "fa fa-adjust",
        url: "/admin/category"
      },
      {
        label: "Publisher",
        icon: "fa fa-home",
        url: "/admin/publisher"
      },
      {
        label: "Author",
        icon: "fa fa-user",
        url: "/admin/author"
      }
    ],
    content: "col-10",
    navbar: {
      logo: "none",
      left: [
        {
          content: (
            <h4>
              <i onClick={() => this.toggleSidebar()} className="fa fa-bars" />
            </h4>
          )
        }
      ],
      right: [
        {
          content: (
            <h5>
              <img
                id="navbar-display"
                src={admin}
                className="rounded-circle"
                alt=""
              />
              Admin
            </h5>
          )
        }
      ]
    },
    users: {
      loading: true,
      body: []
    },
    genres: {
      loading: true,
      body: []
    },
    categories: {
      loading: true,
      body: []
    },
    publishers: {
      loading: true,
      body: []
    },
    authors: {
      loading: true,
      body: []
    }
  };

  constructor() {
    super();
    this.toggleSidebar = this.toggleSidebar.bind(this);
  }

  toggleSidebar() {
    const sidebar = this.state.sidebar === "267px" ? "0px" : "267px";
    const content = this.state.content === "col-10" ? "col-12" : "col-10";
    this.setState({ sidebar, content });
  }

  notifyError = content =>
    toast("Failed to load " + content + " data", {
      type: toast.TYPE.ERROR,
      autoClose: 5000
    });

  async componentDidMount() {
    try {
      await getAllUsers().then(() => {
        let users = this.state.users;
        users.loading = false;
        this.setState({ users });
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        this.notifyError("users");
        let users = this.state.users;
        users.loading = false;

        this.setState({ users });
      }
    }

    try {
      await getAllGenres().then(data => {
        let genres = this.state.genres;
        genres.loading = false;
        genres.body = data.data;
        this.setState({ genres });
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        this.notifyError("categories");
        let categories = this.state.categories;
        categories.loading = false;
        this.setState({ categories });
      }
    }

    try {
      await getAllCategories().then(data => {
        let categories = this.state.categories;
        categories.loading = false;
        categories.body = data.data;
        this.setState({ categories });
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        this.notifyError("genres");
        let categories = this.state.categories;
        categories.loading = false;
        this.setState({ categories });
      }
    }

    try {
      await getAllPublishers().then(data => {
        let publishers = this.state.publishers;
        publishers.loading = false;
        publishers.body = data.data;
        this.setState({ publishers });
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        this.notifyError("genres");
        let publishers = this.state.publishers;
        publishers.loading = false;
        this.setState({ publishers });
      }
    }

    try {
      await getAllAuthors().then(data => {
        let authors = this.state.authors;
        authors.loading = false;
        authors.body = data.data;
        this.setState({ authors });
      });
    } catch (ex) {
      if (ex.response && ex.response.status === 404) {
        this.notifyError("genres");
        let authors = this.state.authors;
        authors.loading = false;
        this.setState({ authors });
      }
    }
  }

  render() {
    return (
      <div className="container-fluid" id="dashboard">
        <div className="row">
          <Sidebar
            status={this.state.sidebar}
            sidebar={this.state.sidebarList}
          />
          <div id="content" className={this.state.content}>
            <Navbar onToggle={this.toggleSidebar} navbar={this.state.navbar} />
            <Switch>
              <ProtectedRoute
                path="/admin/author"
                render={props => (
                  <AdminAuthorContent
                    exact={true}
                    {...props}
                    authors={this.state.authors}
                  />
                )}
              />
              <ProtectedRoute
                path="/admin/publisher"
                render={props => (
                  <AdminPublisherContent
                    exact={true}
                    {...props}
                    publishers={this.state.publishers}
                  />
                )}
              />
              <ProtectedRoute
                path="/admin/category"
                render={props => (
                  <AdminCategoryContent
                    exact={true}
                    {...props}
                    categories={this.state.categories}
                  />
                )}
              />
              <ProtectedRoute
                path="/admin/genre"
                render={props => (
                  <AdminGenreContent
                    exact={true}
                    {...props}
                    genres={this.state.genres}
                  />
                )}
              />
              <ProtectedRoute
                path="/admin/user"
                render={props => (
                  <AdminUserContent
                    exact={true}
                    {...props}
                    users={this.state.users}
                  />
                )}
              />
              <ProtectedRoute
                path="/admin"
                exact={true}
                component={AdminContent}
              />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

export default AdminDashboard;
