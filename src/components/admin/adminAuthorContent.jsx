import React, { Component } from "react";
import _ from "lodash";
import Table from "../common/table/table";
import { ClipLoader } from "react-spinners";
import SearchBox from "../common/searchBox";
import Pagination from "../common/pagination";
import { paginate } from "../../utils/paginate";

class AdminAuthorContent extends Component {
  state = {
    users: [],
    currentPage: 1,
    pageSize: 4,
    searchQuery: "",
    sortColumn: { path: "title", order: "asc" }
  };

  columns = [
    { path: "id", label: "Id" },
    { path: "author_name", label: "Author Name" },
    {
      key: "delete",
      content: book => (
        <div>
          <button
            // onClick={() => this.props.onDelete(movie)}
            className="btn btn-success btn-sm mr-2"
          >
            Edit
          </button>
          <button
            // onClick={() => this.props.onDelete(movie)}
            className="btn btn-danger btn-sm"
          >
            Delete
          </button>
          <button
            // onClick={() => this.props.onDelete(movie)}
            className="btn btn-primary btn-sm ml-2"
          >
            Detail
          </button>
        </div>
      )
    }
  ];

  getPagedData = () => {
    const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
    const allOrders = this.props.orders;
    let filtered = allOrders;
    if (searchQuery)
      filtered = allOrders.filter(m =>
        m.start_location.toLowerCase().startsWith(searchQuery.toLowerCase())
      );

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const orders = paginate(sorted, currentPage, pageSize);

    return { totalCount: filtered.length, data: orders };
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  handleSearch = query => {
    this.setState({ searchQuery: query, currentPage: 1 });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  render() {
    return (
      <div className="m-5 user-table">
        <button className="btn btn-secondary m-3">Add New</button>
        {this.props.authors.loading === true && (
          <div className="text-center">
            <ClipLoader
              sizeUnit={"px"}
              size={150}
              color={"#262626"}
              loading={this.props.authors.loading}
            />
            <br />
            <h1>Fetching Data . . .</h1>
          </div>
        )}

        {this.props.authors.loading === false && (
          <Table
            datas={this.props.authors}
            columns={this.columns}
            sortColumn={this.state.sortColumn}
            onSort={this.handleSort}
          />
        )}
        {this.props.authors.loading === false &&
          this.props.authors.body.length === 0 && (
            <div className="text-center" style={{ padding: "20px" }}>
              <h3>No Data</h3>
            </div>
          )}
      </div>
    );
  }
}

export default AdminAuthorContent;
