import React, { Component } from "react";
import Card from "../common/card/card";

class AdminContent extends Component {
  state = {
    cards: [
      {
        label: "Books",
        icon: "fa fa-book"
      },
      {
        label: "Transaction",
        icon: "fa fa-bitcoin"
      },
      {
        label: "Review",
        icon: "fa fa-users"
      }
    ]
  };
  render() {
    return (
      <div id="admin-content" className="container-fluid">
        <div className="card-deck">
          {this.state.cards.map(card => (
            <Card card={card} />
          ))}
        </div>
      </div>
    );
  }
}

export default AdminContent;
